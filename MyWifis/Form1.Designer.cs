﻿namespace MyWifis
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lblgenerandtxt = new System.Windows.Forms.Label();
            this.btnReport = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtpass = new System.Windows.Forms.TextBox();
            this.txtwifi = new System.Windows.Forms.TextBox();
            this.lblpass = new System.Windows.Forms.Label();
            this.lblssid = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listwifis = new System.Windows.Forms.ListBox();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.languajesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spanishEspañolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishInglesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblgenerandtxt
            // 
            this.lblgenerandtxt.AutoSize = true;
            this.lblgenerandtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgenerandtxt.ForeColor = System.Drawing.Color.Red;
            this.lblgenerandtxt.Location = new System.Drawing.Point(304, 365);
            this.lblgenerandtxt.Name = "lblgenerandtxt";
            this.lblgenerandtxt.Size = new System.Drawing.Size(227, 13);
            this.lblgenerandtxt.TabIndex = 26;
            this.lblgenerandtxt.Text = "Generando TXT, espere un momento...";
            // 
            // btnReport
            // 
            this.btnReport.Location = new System.Drawing.Point(350, 339);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(122, 23);
            this.btnReport.TabIndex = 25;
            this.btnReport.Text = "Generar reporte TXT";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(173, 405);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(101, 13);
            this.linkLabel1.TabIndex = 24;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "pedroruizba.gitlab.io";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 405);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Desarrollado por @pedroruizba ";
            // 
            // txtpass
            // 
            this.txtpass.Location = new System.Drawing.Point(350, 247);
            this.txtpass.Name = "txtpass";
            this.txtpass.Size = new System.Drawing.Size(155, 20);
            this.txtpass.TabIndex = 22;
            // 
            // txtwifi
            // 
            this.txtwifi.Location = new System.Drawing.Point(350, 219);
            this.txtwifi.Name = "txtwifi";
            this.txtwifi.Size = new System.Drawing.Size(155, 20);
            this.txtwifi.TabIndex = 21;
            // 
            // lblpass
            // 
            this.lblpass.AutoSize = true;
            this.lblpass.Location = new System.Drawing.Point(304, 254);
            this.lblpass.Name = "lblpass";
            this.lblpass.Size = new System.Drawing.Size(33, 13);
            this.lblpass.TabIndex = 20;
            this.lblpass.Text = "Pass:";
            // 
            // lblssid
            // 
            this.lblssid.AutoSize = true;
            this.lblssid.Location = new System.Drawing.Point(304, 222);
            this.lblssid.Name = "lblssid";
            this.lblssid.Size = new System.Drawing.Size(28, 13);
            this.lblssid.TabIndex = 19;
            this.lblssid.Text = "Wifi:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(265, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 25);
            this.label3.TabIndex = 18;
            this.label3.Text = "Wifi seleccionada:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(267, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(246, 39);
            this.label2.TabIndex = 17;
            this.label2.Text = "1) Presiona el boton de Cargar Wifis\r\n2) Selecciona una red de la lista\r\n3) Se mo" +
    "strará la contraseña del Wifi seleccionado";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(265, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 25);
            this.label1.TabIndex = 16;
            this.label1.Text = "Instrucciones:";
            // 
            // listwifis
            // 
            this.listwifis.FormattingEnabled = true;
            this.listwifis.Location = new System.Drawing.Point(48, 56);
            this.listwifis.Name = "listwifis";
            this.listwifis.Size = new System.Drawing.Size(196, 277);
            this.listwifis.TabIndex = 15;
            this.listwifis.SelectedIndexChanged += new System.EventHandler(this.listwifis_SelectedIndexChanged);
            // 
            // btnbuscar
            // 
            this.btnbuscar.Location = new System.Drawing.Point(112, 339);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(75, 23);
            this.btnbuscar.TabIndex = 14;
            this.btnbuscar.Text = "Cargar Wifis";
            this.btnbuscar.UseVisualStyleBackColor = true;
            this.btnbuscar.Click += new System.EventHandler(this.btnbuscar_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.languajesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(563, 24);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // languajesToolStripMenuItem
            // 
            this.languajesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spanishEspañolToolStripMenuItem,
            this.englishInglesToolStripMenuItem});
            this.languajesToolStripMenuItem.Name = "languajesToolStripMenuItem";
            this.languajesToolStripMenuItem.Size = new System.Drawing.Size(125, 20);
            this.languajesToolStripMenuItem.Text = "Languajes - idiomas";
            // 
            // spanishEspañolToolStripMenuItem
            // 
            this.spanishEspañolToolStripMenuItem.Name = "spanishEspañolToolStripMenuItem";
            this.spanishEspañolToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.spanishEspañolToolStripMenuItem.Text = "Spanish - Español";
            this.spanishEspañolToolStripMenuItem.Click += new System.EventHandler(this.spanishEspañolToolStripMenuItem_Click);
            // 
            // englishInglesToolStripMenuItem
            // 
            this.englishInglesToolStripMenuItem.Name = "englishInglesToolStripMenuItem";
            this.englishInglesToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.englishInglesToolStripMenuItem.Text = "English -  Ingles";
            this.englishInglesToolStripMenuItem.Click += new System.EventHandler(this.englishInglesToolStripMenuItem_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Fax", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DarkGreen;
            this.label5.Location = new System.Drawing.Point(12, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(512, 14);
            this.label5.TabIndex = 28;
            this.label5.Text = "Si tu pc tiene el idioma ingles cambialo para poder mostrarte las contraseñas cor" +
    "rectamente";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(563, 418);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblgenerandtxt);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtpass);
            this.Controls.Add(this.txtwifi);
            this.Controls.Add(this.lblpass);
            this.Controls.Add(this.lblssid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listwifis);
            this.Controls.Add(this.btnbuscar);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My wifis";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblgenerandtxt;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtpass;
        private System.Windows.Forms.TextBox txtwifi;
        private System.Windows.Forms.Label lblpass;
        private System.Windows.Forms.Label lblssid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listwifis;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem languajesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spanishEspañolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishInglesToolStripMenuItem;
        private System.Windows.Forms.Label label5;
    }
}

