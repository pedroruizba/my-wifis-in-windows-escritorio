﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace MyWifis
{
    public partial class Form1 : Form
    {
        public string note = "";
        public string title_instrucciones = "";
        public string text_instrucciones = "";
        public string title_wifi = "";
        public string button_cargar = "";
        public string button_txt = "";
        public string note_txt = "";
        public string note_txt_generando = "";

        public string replace_text_perfil = "";
        public string replace_text_clave = "";
        public string contain_text_costos = "";
        public string result_text_costos = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            listwifis.Items.Clear();
            string CMDresult = "";
            CMDresult = ExecuteCommand("netsh wlan show profile");
            string modified = CMDresult.Replace(replace_text_perfil, "");

            var sr = new StringReader(modified);
            var sr2 = new StringReader(modified);

            int count = 0, totalwifis = 0;
            string line, line2;
            while ((line2 = sr2.ReadLine()) != null)
            {
                totalwifis++;
            }

            while ((line = sr.ReadLine()) != null)
            {
                count++;
                if (count >= 10 && count <= (totalwifis - 1))
                {
                    listwifis.Items.Add(line);
                }

            }
        }
        public string ExecuteCommand(string _Command)
        {
            //Indicamos que deseamos inicializar el proceso cmd.exe junto a un comando de arranque. 
            //(/C, le indicamos al proceso cmd que deseamos que cuando termine la tarea asignada se cierre el proceso).
            //Para mas informacion consulte la ayuda de la consola con cmd.exe /? 
            System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + _Command);
            // Indicamos que la salida del proceso se redireccione en un Stream
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            //Indica que el proceso no despliegue una pantalla negra (El proceso se ejecuta en background)
            procStartInfo.CreateNoWindow = true;
            //Inicializa el proceso
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();
            //Consigue la salida de la Consola(Stream) y devuelve una cadena de texto
            string result = proc.StandardOutput.ReadToEnd();
            //Muestra en pantalla la salida del Comando
            proc.Close();
            return result;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Navigate to a URL.
            System.Diagnostics.Process.Start("http://pedroruizba.gitlab.io");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            spanish();
            lblgenerandtxt.Visible = false;
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            lblgenerandtxt.Visible = true;
            lblgenerandtxt.Refresh();
            Thread.Sleep(1000);
            string CMDresult3 = "";
            // Set a variable to the Documents path.
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            string CMDresult = "", Report_final = "========Report wifis===========" + Environment.NewLine + Environment.NewLine;
            CMDresult = ExecuteCommand("netsh wlan show profile");
            string modified = CMDresult.Replace(replace_text_perfil, "");

            var sr = new StringReader(modified);
            var sr2 = new StringReader(modified);

            int count = 0, totalwifis = 0;
            string line, line2;
            while ((line2 = sr2.ReadLine()) != null)
            {
                totalwifis++;
            }

            while ((line = sr.ReadLine()) != null)
            {
                count++;
                if (count >= 10 && count <= (totalwifis - 1))
                {
                    CMDresult3 = ExecuteCommand("netsh wlan show profile name=" + '"' + line + '"' + " key =clear");
                    var sr3 = new StringReader(CMDresult3);
                    int count3 = 0;
                    string line3;
                    while ((line3 = sr3.ReadLine()) != null)
                    {
                        count3++;
                        if (count3 > 30 && count3 <= 33)
                        {
                            CMDresult3 = line3;
                        }

                    }
                    CMDresult3 = CMDresult3.Replace(replace_text_clave, "");
                    if (CMDresult3.Contains(contain_text_costos))
                        CMDresult3 = result_text_costos;

                    Report_final += "===========================================" + Environment.NewLine;
                    Report_final += "Wifi: " + line + Environment.NewLine;
                    Report_final += "Pass: " + CMDresult3 + Environment.NewLine;
                    Report_final += "===========================================" + Environment.NewLine;

                }

            }
            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "Report wifis.txt")))
            {
                outputFile.WriteLine(Report_final);
            }
            MessageBox.Show(note_txt_generando, "Important Message");
            lblgenerandtxt.Visible = false;
        }

        private void listwifis_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CMDresult = "", pass = "Buscando...";
            txtwifi.Text = listwifis.SelectedItem.ToString();
            txtpass.Text = pass;
            CMDresult = ExecuteCommand("netsh wlan show profile name=" + '"' + listwifis.SelectedItem.ToString() + '"' + " key =clear");
            var sr = new StringReader(CMDresult);
            int count = 0;
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                count++;
                if (count > 30 && count <= 33)
                {
                    CMDresult = line;
                }

            }
            CMDresult = CMDresult.Replace(replace_text_clave, "");
            if (CMDresult.Contains(contain_text_costos))
                CMDresult = result_text_costos;
            pass = CMDresult;
            txtpass.Text = pass;
        }

        private void spanishEspañolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            spanish();
        }

        private void englishInglesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            english();
        }
        public void spanish()
        {
            note = "Si tu pc tiene el idioma ingles cambialo para poder mostrarte las contraseñas correctamente";
            title_instrucciones = "Instrucciones:";
            text_instrucciones = "1) Presiona el boton de Cargar Wifis \n2) Selecciona una red de la lista \n3) Se mostrará la contraseña del Wifi seleccionado";
            title_wifi = "Wifi seleccionada:";
            button_cargar = "Cargar Wifis";
            button_txt = "Generar reporte TXT";
            note_txt = "Generando TXT, espere un momento...";
            note_txt_generando = "El archivo se creo en el escritorio como: Report wifis.txt.";

            label5.Text = note;
            label1.Text = title_instrucciones;
            label2.Text = text_instrucciones;
            label3.Text = title_wifi;
            btnbuscar.Text= button_cargar;
            btnReport.Text = button_txt;
            lblgenerandtxt.Text = note_txt;

            replace_text_perfil = "    Perfil de todos los usuarios     : ";
            replace_text_clave = "    Contenido de la clave  : ";
            contain_text_costos = "costos";
            result_text_costos = "Esta red no tiene contraseña";

    }
        public void english()
        {
            note = "If your pc has the Spanish language change it to be able to show you the passwords correctly";
            title_instrucciones = "Instructions:";
            text_instrucciones = "1) Press the Load Wifis button \n2) Select a network from the list \n3) The password for the selected Wifi will be displayed";
            title_wifi = "Wifi selected:";
            button_cargar = "Load Wifis";
            button_txt = "Generate TXT report";
            note_txt = "Generating TXT, wait a minute...";
            note_txt_generando = "The file was created on the desktop as: Report wifis.txt";

            label5.Text = note;
            label1.Text = title_instrucciones;
            label2.Text = text_instrucciones;
            label3.Text = title_wifi;
            btnbuscar.Text = button_cargar;
            btnReport.Text = button_txt;
            lblgenerandtxt.Text = note_txt;

            replace_text_perfil = "    All User Profile     : ";
            replace_text_clave = "    Key Content            : ";
            contain_text_costos = "Cost";
            result_text_costos = "This network has no password";

        }
    }
}
